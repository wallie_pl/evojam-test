'use strict';

function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',9):f();}
r(function(){
    var json1, json2;

    // create range array of maxs and mins
    function getRange() {
      var _d = 0,
          _s = 0,
          _r = [],
          _o = [];

      // creating array (_r) based on all data but with columns switched to rows and rows to columns
      for(_d=0; _d<=json1.series[0].data.length-1; _d++) {
        _r[_d] = [];
        for(_s=0; _s<=json1.series.length-1; _s++) {
          _r[_d][_s] = json1.series[_s].data[_d];
        }
      }

      // creating an array (_o) of max values [0] and min values [1] of _r array
      for(var i=0; i<=_r.length-1; i++) {
        _o[i] = [Math.min.apply(Math, _r[i]),Math.max.apply(Math, _r[i])];
      }

      return _o;
    }

    // recalculating all series and adding series data for arearange chart
    function getSeries() {
      var _o = [
          {
            type: 'arearange',
            lineWidth: 0,
            data: getRange(json1.data),
            color: '#e7f6ff',
            enableMouseTracking: false,
            showInLegend: false,
            pointStart: 0,
            fillOpacity: 1
          }
      ];
      for(var item in json1.series) {
        _o.push({
          name: json1.series[item].name,
          lineWidth: 4,
          type: 'line',
          enableMouseTracking: false,
          data: json1.series[item].data,
          color: json1.series[item].color,
          pointStart: 0,
          marker: {
            enabled: true,
            symbol: 'circle',
            radius: 5,
            lineWidth: 3,
            lineColor: '#FFFFFF'
          }
        });
      }
      return _o;
    }

// getting JSON with data for the 'Revenue per week' chart

var xmlhttpChart1 = new XMLHttpRequest();

xmlhttpChart1.onreadystatechange = function() {
  if (xmlhttpChart1.readyState === 4 && xmlhttpChart1.status === 200) {
    var myArr = JSON.parse(xmlhttpChart1.responseText);
    json1 = myArr;
    new Highcharts.Chart({
      'chart': {
        'renderTo': 'GraphContainer1',
        'type': 'arearange',
        'width': 488,
        'height': 190,
        'spacing': 5,
        'spacingTop': 22,
        'spacingBottom': 15,
        'style': {
            'fontFamily': 'Helvetica, Arial, Verdana, sans-serif',
            'fontSize': '12px'
        }
      },
      'title': {
        'text': json1.title,
        'align': 'right',
        'padding': 0,
        'margin': 0,
        'x': 0,
        'y': 0,
        'style': {
            'fontSize': '10px'
        }
      },
      'xAxis': {
          'labels': {
            'align': 'right',
            'x': -10,
            'y': 14,
            'style': {
              'textOverflow': 'none',
              'color': '#05415b',
              'font': '10px Helvetica, Arial, sans-serif'
            },
            'formatter': function () {
              if(this.isFirst) {
                return '';
              }
              else {
                return json1.categories[this.value];
              }
            }
          }
      },
      'yAxis': {
        'floor': json1.floor,
        'ceiling': json1.ceiling,
        'tickPositions': json1.tickPositions,
        'showFirstLabel': false,
        'lineWidth': 1,
        'tickWidth': 1,
        'tickLength': 20,
        'startOnTick': true,
        'offset': -4,
        'title': {
            'text': null
        },
        'labels': {
            'align': 'right',
            'x': -5,
            'y': 15,
            'style': {
                'textOverflow': 'none',
                'color': '#05415b',
                'font': '10px Helvetica, Arial, sans-serif'
            }
        }
      },
      'legend': {
        'symbolPadding': 7,
        'symbolHeight': 7,
        'symbolWidth': 7,
        'symbolRadius': 7,
        'padding': 0,
        'x': -20,
        'y': 1,
        'itemDistance': 25,
        'verticalAlign': 'bottom',
        'itemStyle': {
           'font': '10px Helvetica, Arial, sans-serif'
        }
      },
      'credits': { 'enabled': false },
      'series': getSeries()
    });
  }
};
xmlhttpChart1.open('GET', 'data/chart-1-data.json', true);
xmlhttpChart1.send();

// getting JSON with data for the 'Installations per day' chart

var xmlhttpChart2 = new XMLHttpRequest();

xmlhttpChart2.onreadystatechange = function() {
  if (xmlhttpChart2.readyState === 4 && xmlhttpChart2.status === 200) {
    var myArr = JSON.parse(xmlhttpChart2.responseText);
    json2 = myArr;
    new Highcharts.Chart({
      'chart': {
        'renderTo' : 'GraphContainer2',
        'type': 'column',
        'animation': false,
        'width': 488,
        'height': 180,
        'spacing': 5,
        'spacingTop': 22,
        'style': {
          'fontFamily': 'Helvetica, Arial, Verdana, sans-serif',
          'fontSize': '12px'
        }
      },
      'title': {
          'text': json2.title,
          'align': 'right',
          'padding': 0,
          'margin': 0,
          'x': 0,
          'y': 0,
          'style': {
            'fontSize': '10px'
          }
      },
      'xAxis': {
          'labels': {
            'align': 'right',
            'x': -5,
            'y': 14,
            'style': {
              'textOverflow': 'none',
              'color': '#05415b',
              'font': '10px Helvetica, Arial, sans-serif'
            },
            'formatter': function () {
              return json2.categories[this.value];
            }
          }
      },
      'yAxis': {
          'floor': json2.floor,
          'ceiling': json2.ceiling,
          'tickPositions': json2.tickPositions,
          'showFirstLabel': false,
          'lineWidth': 1,
          'tickWidth': 1,
          'tickLength': 20,
          'startOnTick': true,
          'offset': -4,
          'title': {
            'text': null
          },
          'labels': {
            'align': 'right',
            'x': -5,
            'y': 15,
            'style': {
              'textOverflow': 'none',
              'color': '#05415b',
              'font': '10px Helvetica, Arial, sans-serif'
            }
          }
      },
      'legend': {
        'symbolPadding': 7,
        'symbolHeight': 7,
        'symbolWidth': 7,
        'symbolRadius': 7,
        'padding': 0,
        'x': -20,
        'itemDistance': 25,
        'verticalAlign': 'bottom',
        'itemStyle': {
           'font': '10px Helvetica, Arial, sans-serif'
        }
      },
      'credits': {
          'enabled': false
      },
      'plotOptions': {
          'column': {
              'groupPadding': 0.25
          }
      },
      'series': json2.series
    });
  }
};
xmlhttpChart2.open('GET', 'data/chart-2-data.json', true);
xmlhttpChart2.send();

});
